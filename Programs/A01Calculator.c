/***************************************************************************
*File			: A01Calculator.c
*Description	: Program to simulate a commercial calculator
*Author		: Prabodh C P
*Compiler		: gcc compiler, Ubuntu 22.04
*Date			: 10 August 2022
***************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<math.h>

/***************************************************************************
*Function			: 	main
*Input parameters	:	no parameters
*RETURNS			:	0 on success
***************************************************************************/

int main()
{
	int iChoice, iOperand1, iOperand2;
	char cOperator;
	
	for(;;)
	{
		printf("\nEnter the arithmetic expression(Do not add spaces in the expression)\n");
		scanf("%d%c%d", &iOperand1, &cOperator, &iOperand2);
		switch(cOperator)
		{
			case '+': 	printf("\nResult = %d", iOperand1 + iOperand2);
						break;
					
			case '-':	printf("\nResult = %d", iOperand1 - iOperand2);
						break;
			
			case '*':	printf("\nResult = %d", iOperand1 * iOperand2);
						break;
						
			case '/':	printf("\nResult = %g", (float)iOperand1 / iOperand2);
						break;
			case '%':	printf("\nResult = %d", iOperand1 % iOperand2);
						break;
					
		}
		printf("\nPress 1 to continue and 0 to quit : ");
		scanf("%d", &iChoice);
		if(0==iChoice)
		{
			break;
		}
	}
	return 0;		
}



