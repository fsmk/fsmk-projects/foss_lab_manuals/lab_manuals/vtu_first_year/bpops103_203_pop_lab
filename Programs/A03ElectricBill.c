/***************************************************************************
*File			: A03ElectricBill.c
*Description	: Program to perform a binary search on 1D Array
*Author		: Prabodh C P
*Compiler		: gcc compiler, Ubuntu 22.04
*Date			: 10 August 2022
***************************************************************************/
#include<stdio.h>
#include<stdlib.h>
/***************************************************************************
*Function		: 	main
*Input parameters	:	no parameters
*RETURNS		:	0 on success
***************************************************************************/
int main(void)
{
	char cName[30];
	int iUnits;
	const int iMinCharge = 100;
	const double dSlab1 = 0.8;
	const double dSlab2 = 0.9;
	const double dSlab3 = 1.0;
	const double dSurcharge = 0.15;
	double dBillAmount = 0.0;

	printf("\nEnter the name of the customer : "); 		scanf("%s", cName);
	printf("\nEnter the number of units consumed : "); 	scanf("%d", &iUnits);	
	dBillAmount += iMinCharge;
	if(iUnits <= 200)
	{
		dBillAmount += iUnits*dSlab1;
	}
	else if(iUnits > 200 && iUnits <= 300)
	{
		dBillAmount += (200*dSlab1)+((iUnits-200)*dSlab2);
	}
	else
	{
		dBillAmount += (200*dSlab1)+(100*dSlab2)+((iUnits-300)*dSlab3);
	}
	if(dBillAmount > 400)
	{
		dBillAmount += dBillAmount * dSurcharge;
	}
	printf("\nElectricity Bill\n===================================");
	printf("\nCustomer Name\t: %s", cName);
	printf("\nUnits Consumed\t: %d", iUnits);
	printf("\nBill Amount\t: %0.2lf Rupees\n\n", dBillAmount);
	return 0;
}
