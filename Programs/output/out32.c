/***************************************
Enter Customer name:SHAM

Enter number of units consumed: 150
ELECTRIC BILL
Name :                   SHAM
Units Consumed :         150 units
Bill Minimum            =  100.00 Rs
	150 *  80 paise     =  120.00
Total Bill Charges      =  220.00 Rs
===================================

Enter Customer name:BABU

Enter number of units consumed: 260
ELECTRIC BILL
Name :                   BABU
Units Consumed :         260 units
Bill Minimum            =  100.00 Rs
	200 *  80 paise     =  160.00
	 60 *  90 paise     =   54.00
Total Bill Charges      =  314.00 Rs
===================================

Enter Customer name:RAMU

Enter number of units consumed: 555
ELECTRIC BILL
Name :                   RAMU
Units Consumed :         555 units
Bill Minimum            =  100.00 Rs
	200 *  80 paise     =  160.00 Rs
	100 *  90 paise     =   90.00 Rs
	255 * 100 paise     =  255.00 Rs
Surcharge Amount        =  104.36 Rs
Total Bill Charges      =  695.75 Rs
===================================

***************************************/

